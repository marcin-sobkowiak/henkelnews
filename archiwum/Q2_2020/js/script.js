$(document).ready(function(){
    $(".z-box-1 .btn-2").click(function(){
        $(this).parent().parent().parent().find('.s-content-box-1').fadeToggle();
        $(this).toggleClass('active');
        return false;
    });
});

$(document).ready(function(){
    $(".z-content-box-3 .btn-1").click(function(){
        $('html, body').animate({
            scrollTop: $('.nav-tabs-1').offset().top - 150
        }, 1000);
        return false;
    });
});

$(function () {
    $('.nav-tabs-1 a').click(function () {
        $('.nav-tabs-1 li').removeClass('current');
        $(this).parent().addClass('current');
        $('.cont-box').removeClass('active');
        $('.cont-' + ($(this).attr('class'))).addClass('active');
        return false;
    });
});

$(document).ready(function(){
    $('input').iCheck({
    checkboxClass: 'icheckbox',
    radioClass: 'iradio'
    });
});

$(function(){
    var logoRsp = $('header .logo').html();
    $('.header-rsp .logo-rsp a').html(logoRsp);
    
    var dataRsp = $('header .langs-box').html();
    $('.header-rsp .data-rsp').html("<div class='langs-box'>" + dataRsp + "</div>");    
    
    $(".hamburger").click(function(){
        $(this).toggleClass('is-active');
        $('.menu-rsp').fadeToggle(200);
    });
    var mainMenu = $('.main-menu ul').html();
    $('.menu-rsp nav').html('<ul>' + mainMenu + '</ul>');
    $('.menu-rsp nav ul > li > ul').parent().find('a:first').append('<span class="btn-slide-down">&#9660;</span>');
    $('.main-menu ul > li > ul').parent().find('a:first').append('<span class="btn-slide-down">&#9660;</span>');
    $('.btn-slide-down').parent().parent().addClass('li-submenu');
    $('.btn-slide-down').click(function(){
        $(this).parent().next().stop().slideToggle(200);
        return false;
    });
    $('.hamburger').click(function(){
        $('.menu-box').toggleClass('active');
        return false;
    });
});

$(function(){ 	
    var config = {
      '.chosen-select': {}
    }
    for (var selector in config) {
       $(selector).chosen(config[selector]);
    }
});

$(function(){
    $('.banner ul').bxSlider({
        nextSelector: '.banner .next-box',
        prevSelector: '.banner .prev-box',        
        mode: 'fade',
        captions: true,
        controls: true,
        auto: true,
        pause: 3000,
        touchEnabled: false,
        stopAutoOnClick: true   
    });
});

$(function(){
    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('body').addClass('body-active');
        } else {
            $('body').removeClass('body-active');
        }
    });
});

$(function(){
    $('.scroll-top').click(function(){
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
        return false;
    });
});
